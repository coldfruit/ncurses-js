const top_cmd = async () => {
    let {
        newterm
    } = ncurses;

    let term = await newterm('#terminal');
    let username = 'foo';
    let hostname = 'bar';
    let path = '~';
    
    term.printf("%s@%s:%s$", username, hostname, path);
    document.body.addEventListener('keypress', (kpev) => {
        try {
            switch(kpev.key) {
                case 'Enter':
                    term.putc('\n');
                    term.printf("%s@%s:%s$", username, hostname, path);
                    break;
                default:
                    term.putc(kpev.key);
                    break;
            }
        } catch (e) {
            console.log(`Error: ${e} caused by invalid key press: ${kpev.key}`);
        }
    })
};

top_cmd();