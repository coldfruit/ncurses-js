# How to run the example

0. Run `npm install`
1. Run `npm run build`
2. Run `npm -g i http-server`
3. Run `http-server`

Obviously you can use any other simple static server like python's 
SimpleHTTPServer instead.
