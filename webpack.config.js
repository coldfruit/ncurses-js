const path = require('path');

module.exports = {
  entry: './src/index.ts',
  output: {
    filename: 'ncurses.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'ncurses'
  },
  watch: true,
  devtool: 'inline-source-map',
  module: {
      rules: [
          {
              test: /\.css$/i,
              use: ['style-loader', 'css-loader']
          },
          {
            test: /\.tsx?$/i,
            use: 'ts-loader'
          }
      ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  }
};