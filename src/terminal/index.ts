import './terminal.css';
import { sprintf as printf } from 'sprintf-js';

const TERM_WIDTH = 80;
const TERM_HEIGHT = 24;

class Terminal {
    width: any;
    height: any;
    cells: TerminalCell[][];
    rootEl: any;
    cursorCoord: { x: number; y: number; };

    constructor(rootEl: Element, width: number, height: number) {
        this.width = width;
        this.height = height;
        this.cells = [];
        this.rootEl = rootEl;
        this.cursorCoord = {x:0,y:0};
        for(let y = 0; y < height; y++) {
            for(let x = 0; x < width; x++) {
                if(!this.cells[x]) this.cells[x] = [];
                let newTermEl = document.createElement('div');
                this.cells[x][y] = new TerminalCell(' ', newTermEl);
                newTermEl.classList.add('ncurses--cell');
                newTermEl.setAttribute('data-coord-x', x.toString());
                newTermEl.setAttribute('data-coord-y', y.toString());
                rootEl.appendChild(newTermEl);
            }
        }
    }

    _increment_cursor() {
        
        this.cursorCoord.x++;
        if(this.cursorCoord.x >= this.width) {
            this.cursorCoord.x = 0;
            this.cursorCoord.y++;
            if(this.cursorCoord.y >= this.height) {
                this.cursorCoord.y = 0;
            }
        }
    }

    putc(c: string|number) {
        // runtime type checking as well because idk if downstream is 
        // using TypeScript or not
        if(typeof c !== 'string' && typeof c !== 'number') {
            throw "input to putc must be string or ASCII-encoded number";
        }
        if(typeof c === 'string' && c.length !== 1) {
            throw "input to putc must not be >1 character long";
        }
        if(typeof c === 'number') {
            c = String.fromCharCode(c);
        }
        this.cells[this.cursorCoord.x][this.cursorCoord.y].setChar(c);
        this._increment_cursor();
    }

    printf(fmt:string, ...args:any) {
        console.log("Printf was called");
        let str = printf(fmt, ...args);
        console.debug("Printf returned: ", str);
        for(let i = 0; i < str.length; i++) {
            this.putc(str[i]);
        }
    }

    refresh() {
        for(let x = 0; x < this.width; x++) {
            for(let y = 0; y < this.height; y++) {
                this.cells[x][y].blit();
                if(this.cursorCoord.x === x && this.cursorCoord.y === y) {
                    // causes the cursor "cell" to appear to "flash"
                    this.cells[x][y].toggle_cursor();
                } else if (this.cells[x][y].cursor_lit()) {
                    this.cells[x][y].disable_cursor();
                }
            }
        }
        requestAnimationFrame(this.refresh.bind(this));
    }
}

class TerminalCell {
    character: any;
    dirty: boolean;
    element: any;
    last_toggle: number;
    cursor: boolean;
    constructor(c:string, el: Element) {
        this.character = c;
        this.dirty = false;
        this.element = el;
        this.last_toggle = Date.now();
        this.cursor = false;
    }

    setChar(c: string) {
        this.character = c;
        this.dirty = true;
    }
    
    toggle_cursor() {
        if(Date.now() - this.last_toggle >= 500) {
            this.cursor = !this.cursor;
            this.last_toggle = Date.now();
        }
    }

    cursor_lit() {
        return this.cursor;
    }

    disable_cursor() {
        this.cursor = false;
    }

    enable_cursor() {
        this.cursor = true;
    }

    blit() {
        if(this.dirty) {
            console.debug('element was dirty, refreshing with:', this.character);
            this.element.innerText = this.character;
            this.dirty = false;
        }
        if(this.cursor) {
            this.element.style['background-color'] = this.element.style['color'];
        } else {
            this.element.style['background-color'] = '#0';
        }
    }
};

/**
 * newterm is analogous to `initscr` or `newterm` in ncurses, but takes in three
 * arguments to support initialising the terminal simulator at the same time as
 * setting up the library.
 * 
 * @param {string} terminalSelector a CSS selector pointing to where to draw the terminal
 * @param {number} width the width (in columns) of the terminal simulator
 * @param {number} height the height (in rows) of the terminal simulator
 */
const newterm = async (terminalSelector:string, width=TERM_WIDTH, height=TERM_HEIGHT) => {
    let terminalEl:HTMLElement = document.querySelector(`${terminalSelector}`);
    terminalEl.innerHTML = "";
    terminalEl.classList.add('ncurses--term');
    terminalEl.style.backgroundColor = '#000';
    terminalEl.style.gridTemplateColumns = "12px ".repeat(width);
    terminalEl.style.gridTemplateRows = "22px ".repeat(height);
    let term = new Terminal(terminalEl, width, height);

    term.refresh();

    requestAnimationFrame(term.refresh.bind(term));

    return term;
}

export {
    newterm,
    Terminal
}